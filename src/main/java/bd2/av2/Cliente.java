package bd2.av2;

import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tab_cliente")
public class Cliente {

	@Id
	@Column(name = "cpf", columnDefinition = "char(11)", nullable = false)
	private String cpf;

	@Column(name = "nome", length = 40, nullable = false)
	private String nome;

	@Embedded
	@Column(name = "endereco")
	private Endereco endereco;

	@OneToMany(mappedBy = "cliente", fetch = FetchType.EAGER)
	@ElementCollection
	@CollectionTable(name = "tab_cliente_produto", joinColumns = @JoinColumn(name = "cpf"))
	@Column(name = "produtos")
	private List<Produto> produtos;

	@ElementCollection
	@CollectionTable(name = "tab_cliente_telefone", joinColumns = @JoinColumn(name = "cpf"))
	@Column(name = "telefone", columnDefinition = "char(12)")
	private List<String> telefone;

	public Cliente() {
	}

	public Cliente(String cpf, String nome, Endereco endereco, List<Produto> produtos, List<String> telefone) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.endereco = endereco;
		this.produtos = produtos;
		this.telefone = telefone;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public List<String> getTelefone() {
		return telefone;
	}

	public void setTelefone(List<String> telefone) {
		this.telefone = telefone;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((produtos == null) ? 0 : produtos.hashCode());
		result = prime * result + ((telefone == null) ? 0 : telefone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (cpf == null) {
			if (other.cpf != null)
				return false;
		} else if (!cpf.equals(other.cpf))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (produtos == null) {
			if (other.produtos != null)
				return false;
		} else if (!produtos.equals(other.produtos))
			return false;
		if (telefone == null) {
			if (other.telefone != null)
				return false;
		} else if (!telefone.equals(other.telefone))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Cliente [cpf=" + cpf + ", nome=" + nome + ", produtos=" + produtos + ", telefone=" + telefone + "]";
	}

}
