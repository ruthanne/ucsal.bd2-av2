package bd2.av2;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tab_bolo")
public class Bolo extends Produto{

	@OneToMany
	@Column(name = "quantidade_fatias", nullable = false)
	private Integer quantidadeFatias;

	@Column(name = "tema", columnDefinition = "char(3)", nullable = false)
	private TemaBoloEnum tema;

	public Bolo() {
	}

	public Bolo(Integer quantidadeFatias, TemaBoloEnum tema) {
		super();
		this.quantidadeFatias = quantidadeFatias;
		this.tema = tema;
	}

	public Integer getQuantidadeFatias() {
		return quantidadeFatias;
	}

	public void setQuantidadeFatias(Integer quantidadeFatias) {
		this.quantidadeFatias = quantidadeFatias;
	}

	public TemaBoloEnum getTema() {
		return tema;
	}

	public void setTema(TemaBoloEnum tema) {
		this.tema = tema;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((quantidadeFatias == null) ? 0 : quantidadeFatias.hashCode());
		result = prime * result + ((tema == null) ? 0 : tema.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bolo other = (Bolo) obj;
		if (quantidadeFatias == null) {
			if (other.quantidadeFatias != null)
				return false;
		} else if (!quantidadeFatias.equals(other.quantidadeFatias))
			return false;
		if (tema != other.tema)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Bolo [quantidadeFatias=" + quantidadeFatias + ", tema=" + tema + "]";
	}

}
