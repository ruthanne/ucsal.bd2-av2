package bd2.av2;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "tab_doce")
public class Doce extends Produto {

	@Column(name = "qtd_minima", nullable = false)
	private Integer qtdMinima;

	public Doce() {
	}

	public Doce(Integer qtdMinima) {
		super();
		this.qtdMinima = qtdMinima;
	}

	public Integer getQtdMinima() {
		return qtdMinima;
	}

	public void setQtdMinima(Integer qtdMinima) {
		this.qtdMinima = qtdMinima;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((qtdMinima == null) ? 0 : qtdMinima.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Doce other = (Doce) obj;
		if (qtdMinima == null) {
			if (other.qtdMinima != null)
				return false;
		} else if (!qtdMinima.equals(other.qtdMinima))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Doce [qtdMinima=" + qtdMinima + "]";
	}

}
