package bd2.av2;

public enum TemaBoloEnum {

	INFANTIL("IFT"), ADOLESCENTE("ADL"), ADULTO("ADT");

	String codigo;

	private TemaBoloEnum(String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public static TemaBoloEnum valueOfCodigo(String codigo) {
		for (TemaBoloEnum temaBolo : values()) {
			if (temaBolo.getCodigo().equals(codigo)) {
				return temaBolo;
			}
		}
		throw new IllegalArgumentException();
	}
}
