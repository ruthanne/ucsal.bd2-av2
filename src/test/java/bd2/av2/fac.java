package bd2.av2;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import org.hibernate.query.NativeQuery;

public class fac {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		try {
			emf = Persistence.createEntityManagerFactory("pu");
			EntityManager em = emf.createEntityManager();

			popular(em);

			// limpando o cache do em
			em.clear();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (emf != null) {
				emf.close();
			}
		}
	}

	private static void atualizarNomeBolo(EntityManager em, String nome) {
		em.getTransaction().begin();
		NativeQuery query = (NativeQuery) em.createNativeQuery("update tab_bolo set nome=:nome where nome=:nome");
		query.setParameter("nome", nome);
		query.executeUpdate();
		em.getTransaction().commit();
	}

	private static Bolo criarBolo(EntityManager em, Integer quantidadeFatias, TemaBoloEnum tema) {
		Bolo bolo = new Bolo();
		bolo.setCodigo(codigo);
		bolo.setNome(nome);
		bolo.setDescricao(descricao);
		bolo.setQuantidadeFatias(quantidadeFatias);
		bolo.setTema(tema);
		bolo.setValorUnitario(valorUnitario);
		em.persist(bolo);
		return bolo;
	}

	private static Cliente criarCliente(EntityManager em, String cpf, String nome, Endereco endereco,
			List<Produto> produtos, List<String> telefone) {
		Cliente cliente = new Cliente();
		cliente.setCpf(cpf);
		cliente.setNome(nome);
		cliente.setEndereco(endereco);
		cliente.setProdutos(produtos);
		cliente.setTelefone(telefone);

		em.persist(cliente);
		return cliente;
	}

	private static void popular(EntityManager em) {
		em.getTransaction().begin();

		Cliente cliente1 = criarCliente(em, "93847593023", "Joaquim", endereco, produtos, telefone);

		em.persist(cliente1);

		em.getTransaction().commit();
	}

}
